<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UsersController extends Controller
{
    public function index($id)
    {
    	$user = DB::table('users')->where('users.id',$id)
       ->select('users.name','users.title','users.description','users.id','users.avatar','users.url')
        ->get();
        $posts = DB::table('posts')
		->join('users', 'user_id', '=', 'users.id')
		->get();
    	return view('users',['posts' => $posts , 'users' =>$user]);
    }
    public function edit($id)
    {
    	// mengambil data dari table posts
    	$user = DB::table('users')->where('users.id',$id)->get();

    	// mengirim data posts ke view index
    	return view('edit',['user' => $user]);
 
    }

    public function update(Request $request)
    {
        $file = $request->file('file');
        DB::table('users')->where('id',$request->id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'url' => $request->url,
            'avatar' => $file->getClientOriginalName()
        ]);

        $tujuan_upload = 'image';
	    $file->move($tujuan_upload,$file->getClientOriginalName());
        return redirect('/posts');
    }
}
