<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class KomentarPostsController extends Controller
{
    public function tampil($id)
	{
        $komentar = DB::table('komentar_posts')->where('post_id',$id)
        ->join('users', 'user_id', '=', 'users.id')
        ->join('posts', 'post_id', '=', 'posts.id')
        ->get();

        $posts = DB::table('posts')->where('posts.id',$id)
		->join('users', 'user_id', '=', 'users.id')
		->get();

        $postid =$id;
		return view('komentar',['posts' => $posts, 'komentar' => $komentar,'postid' => $postid ]);
	
    }
    
    public function komen(Request $request)
    {	
        DB::table('komentar_posts')->insert([
            'user_id' => $request->userid,
            'post_id' => $request->postid,
            'comment' => $request->comment,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
        return redirect('/posts');
     
    }
    public function like(Request $request)
    {	
        $jumlah = $request->totallike +1 ;
       

        DB::table('posts')->where('id',$request->postid)->update([
            'likes' => $jumlah
        ]);
        return redirect('/posts');
    }
}
