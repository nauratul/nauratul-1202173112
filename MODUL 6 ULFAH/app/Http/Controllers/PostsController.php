<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostsController extends Controller
{
    public function index()
    {
    	$posts = DB::table('posts')
		->join('users', 'user_id', '=', 'users.id')
		->join('komentar_posts','komentar_posts.id', '=','posts.id')
		->select('users.name','users.title','users.description','users.avatar','users.url','posts.image',
		'posts.id','posts.caption','users.id as userid')
		->get();
    	return view('index',['posts' => $posts]);
		
		
	}
	
	
		public function tambah($id)
	{
		$posts = DB::table('users')->where('users.id',$id)->get();

		// memanggil view tambah
		return view('tambah',['posts' => $posts]);
	
	}
	public function post(Request $request)
	{	
	$file = $request->file('file');
	$image = $file->getClientOriginalName();
	DB::table('posts')->insert([
		'user_id' => $request->id,
		'caption' => $request->caption,
		'image' => $file->getClientOriginalName(),
		'likes' => '0',
		'created_at' => date("Y-m-d H:i:s"),
		'updated_at' => date("Y-m-d H:i:s")
	]);
	// alihkan halaman ke halaman 
	$tujuan_upload = 'image';
	$file->move($tujuan_upload,$file->getClientOriginalName());
	return redirect('/posts');
 
}
}
