@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                    <div class="card-header">
                        <img src ="{{ Auth::user()->avatar}}" alt="profile" class="rounded-circle"style="width: 1cm; height: 1cm;">
                        <b>{{ Auth::user()->name}}</b>
                    </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <center>
                        <img src="{{ Auth::user()->title}}" style="width:100% ">
                    </center>
                </div>
                <div class="card-header">
                    <b>{{ Auth::user()->email}}</b>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
