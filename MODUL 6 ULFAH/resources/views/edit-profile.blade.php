@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
                    <form method="POST">
                        @csrf

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                            <h1>Edit Profile</h1>
                                <label for="title" class="col-form-label pl-3">{{ __('Title') }}</label>
                                <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" required autocomplete="title" autofocus>

                                @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                <label for="description" class="col-form-label pl-3">{{ __('Description') }}</label>
                                <input id="description" type="description" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}" required autocomplete="description">

                                @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                <label for="url" class="col-form-label pl-3">{{ __('URL') }}</label>
                                <input id="url" type="text" class="form-control @error('url') is-invalid @enderror" name="url" required autocomplete="new-url">

                                @error('url')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="col-md-8">
                                <label for="profile-image" class="col-form-label pl-3">{{ __('Profile Image') }}</label><br>
                                <input id="profile-image" type="file" name="profile-image" required autocomplete="profile-image">
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mt-4">
                            <div class="col-md-8">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Save Profile') }}
                                </button>
                            </div>
                        </div>
                    </form>
    </div>
</div>
@endsection
