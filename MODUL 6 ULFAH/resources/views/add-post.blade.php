@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
        <form method="POST">
            @csrf

            <div class="form-group row justify-content-center">
                <div class="col-md-8">
                    <h1>Add New Post</h1>
                    <label for="caption" class="col-form-label pl-3">{{ __('Post Caption') }}</label>
                    <input id="caption" type="text" class="form-control @error('caption') is-invalid @enderror" name="caption" value="{{ old('caption') }}" required autocomplete="caption" autofocus>

                    @error('caption')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row justify-content-center">
                <div class="col-md-8">
                    <label for="post-image" class="col-form-label pl-3">{{ __('Post Image') }}</label><br>
                    <input id="post-image" type="file" name="post-image" required autocomplete="post-image">
                </div>
            </div>

            <div class="form-group row justify-content-center mt-4">
                <div class="col-md-8">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Add New post') }}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
@endsection
